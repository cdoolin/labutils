

import multiprocessing
import numpy as np
from scipy import signal
import argparse
import os
import time
import sys



cal = np.r_[2.5e-3, -1.04]
def do_cal(a):
    return cal[0] * (a & 0x3fff) + cal[1]

def do_demods_iir(pulses, F0s, DFs, downsample, RATE, order=2):
    t = np.arange(pulses.shape[1]) / RATE
    filters = [signal.butter(order, 2. * DF / RATE) for DF in DFs]
    oscs =  [np.exp(-1j*2.*np.pi * F0 * t) for F0 in F0s]
    R2s = [np.zeros(pulses.shape[1] // downsample) for f in F0s]
    norm = 1. / len(pulses) if len(pulses) > 0 else 1.

    for p in pulses:
        pcal = do_cal(p)
        for i in range(len(F0s)):
            Y = pcal * oscs[i]
            
            Yf = signal.sosfilt(filters[i], Y)
            Yf = signal.sosfilt(filters[i], Yf[::-1])[::-1]
            Yf = Yf[::downsample]
            # Yf = signal.lfilter(filters[i][0], filters[i][1], Y)[::downsample]

            R2s[i] += np.real(Yf * np.conj(Yf)) * norm

    return np.asarray(R2s), t[::downsample]


def do_demods_fir(pulses, F0s, DFs, downsample, RATE, order=1):
    hMs = [round(RATE*1*order/DF) for DF in DFs] 
    filters = [signal.firwin(2*hM+1, 2.* DF / RATE) for hM, DF in zip(hMs, DFs)]

    t = np.arange(pulses.shape[1]) / RATE
    oscs =  [np.exp(-1j*2.*np.pi * F0 * t) for F0 in F0s]
    R2s = [np.zeros(pulses.shape[1] // downsample) for f in F0s]
    norm = 1. / len(pulses) if len(pulses) > 0 else 1.

    for p in pulses:
        pcal = do_cal(p)
        for i in range(len(F0s)):
            Y = pcal * oscs[i]
            Yf = signal.resample_poly(Y, 1, downsample, window=filters[i])

            R2s[i] += np.real(Yf * np.conj(Yf)) * norm

    return np.asarray(R2s), t[::downsample]

def do_demod_and_save(fname, i, splits, F0s, DFs, downsample, RATE, order=2,  ftype='iir', savet=True):
    funcs = { 'iir': do_demods_iir, 'fir': do_demods_fir}
    if ftype not in funcs:
        print("unknown filter type '%s'" % ftype)
        return

    data = np.load(fname, mmap_mode='r')

    ppersplit = len(data) // splits
    data = data[i*ppersplit:(i+1)*ppersplit]
    
    R2s, t = funcs[ftype](data, F0s=F0s, DFs=DFs, downsample=downsample, RATE=RATE, order=order)

    basename, _ = os.path.splitext(fname)
    np.save("%s_%dof%d_R2s" % (basename, i+1, splits), R2s)
    if savet and i == 0:
        np.save("%s_t" % (basename,), t)

    return "ok"




# parser.add_argument('--sum', dest='accumulate', action='store_const',
#                     const=sum, default=max,
#                     help='sum the integers (default: find the max)')



parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('files', type=str, nargs='+',
    help='files to demodulate')

parser.add_argument('--freqs', '-f', type=float, nargs='+',
    help='frequencies and bandwidths to demodulate at')

parser.add_argument('--threads', '-t', type=int, default=4,
    help='number of processes to spawn to do the demodulation')
parser.add_argument('--split', '-s', type=int, default=1,
    help='split each file and process independently.  will enable better multithreading')


parser.add_argument('--rate', type=float, default=80e6,
    help='samplerate the data is collected at')
    
parser.add_argument('--downsample', type=int, default=1000,
    help='downsample to demodulated data by this amount')

parser.add_argument('--order', type=int, default=1,
    help='filter "order". not actually order')

parser.add_argument('--type', type=str, default="fir",
    help='filter type. fir (default) or iir')





if __name__ == "__main__":
    args = parser.parse_args()

    F0s = args.freqs[0::2]
    DFs = args.freqs[1::2]
    if len(F0s) != len(DFs):
        print("Number of frequencies should match number of bandwidths")

    pool = multiprocessing.Pool(args.threads)

    kwargs = {
        'splits': args.split,
        'F0s': F0s,
        'DFs': DFs,
        'downsample': args.downsample,
        'RATE': args.rate,
        'order': args.order,
        'ftype': args.type,
    }

    # if any files are directories use all files in that directory
    files = []
    for f in args.files:
        if os.path.isdir(f):
            files += [os.path.join(f, fi) for fi in os.listdir(f)]
        else:
            files.append(f)

    results = []
    for fn in files:
        for i in range(args.split):
            kw = {'fname': fn, 'i': i, **kwargs}
            r = pool.apply_async(do_demod_and_save, (), kw)
            results.append(r)

    print("submitted %d jobs to %d threads" % (len(results), args.threads))

    waiting = True
    while waiting:
        ndone = sum([r.ready() for r in results])
        sys.stdout.write("\r%d / %d jobs done." % (ndone, len(results)))
        sys.stdout.flush()

        waiting = ndone < len(results)
        time.sleep(2)

    print("")

    nsucc = sum([r.successful() for r in results])
    print("%d / %d jobs finished successfully." % (nsucc, len(results)))

    if nsucc < len(results):
        gets = [r.get() for r in results]
        print(gets)




    

