
#!/usr/bin/env python

from setuptools import setup

setup(
    name = 'labutils',
    version = '0.1',
    author = "Callum Doolin",
    author_email = "doolin@ualberta.ca",

    description = "A collection of python functions and programs useful for doing physics lab work",
    packages = ['labutils'],
    license = "BSD",
    # include_package_data=True,
    scripts = [
        'scripts/do-threaded-demod.py',
    ],

    install_requires = [
        'numpy',
        'scipy',
    ],
)