# utilities for analyzing data
# depends on numpy and scipy
VERSION=4

# 2: fixed bug where midpoint (midi) was calculated incorrectly
# 3: added tlb-6712 background function
# 4: can pass already loaded data in as fname

from numpy import *
from scipy import signal


tlb6712bg = poly1d(array([  1.28247318e-31,  -1.29012462e-28,  -7.09275167e-26,
         1.11415516e-23,   4.42466042e-20,   3.59024183e-17,
         1.16400174e-14,  -8.71247664e-12,  -1.64946500e-08,
        -1.22869244e-05,  -1.96438743e-03,   6.91676196e+00,
         7.61267295e+03,  -4.48115154e+06]))

tlb6712bg2 = poly1d(array([-4.85198275e-14,  6.94000341e-11,  1.26588223e-08, 
                           -2.86512744e-05, -2.23540834e-02,  5.62195368e+00,  
                            1.92524731e+04, -7.98476357e+06]))


def load_wavevolt(fname, start=765., stop=781., trans_cut=1., wave_cut=.0015, tlb6712=False, tlb67122=False, cal=[1., 0.]):
    try:
        ext = fname[-3:]
    except:
        ext = "data"
    
    if ext == "data":
        trans, wls = fname
    elif ext == "npy":
        trans, wls = load(fname).T
    else:
        trans, wls = loadtxt(fname, dtype='f4').T
        
    trans = cal[0] * trans + cal[1]

    # smooth volt wavelength data
    if wave_cut < 1.:
        window = signal.firwin(int(1e4) + 1, wave_cut)
        wls = signal.fftconvolve(wls, window, mode='same')
        del window

    if trans_cut < 1.:
        window = signal.firwin(int(1e4) + 1, trans_cut)
        trans = signal.fftconvolve(trans, window, mode='same')
        del window


    # convert volts to wavelengths
    wls = start + wls / 10. * (stop - start)
    
    # normalize if needed
    if tlb6712:
        trans = trans / tlb6712bg(wls)
    if tlb67122:
        trans = trans / tlb6712bg2(wls)
        

    # find approx mid point
    midi = int(mean(arange(len(wls))[wls > (start + stop) / 2.]))

    # split forward and backwards scans and sort data for interpolation
    sortf = argsort(wls[:midi])
    wlf = wls[:midi][sortf]
    trf = trans[:midi][sortf]

    sortr = argsort(wls[midi:])
    wlr = wls[midi:][sortr]
    trr = trans[midi:][sortr]

    # force garbage collection else returned functions will
    # keep everything in memory
    del  trans, wls, sortf, sortr, midi

    # reinterpolate
    def scanf(wls):
        return interp(wls, wlf, trf)
    scanf.n = len(wlf)
    scanf.wl = wlf
    scanf.tr = trf

    def scanr(wls):
        return interp(wls, wlr, trr)
    
    scanr.n = len(wlr)
    scanr.wl = wlr
    scanr.tr = trr

    return scanf, scanr


def make_interpolator(xs, ys):
    def func(x):
        return interp(x, xs, ys)
    return func

def load_wavevolt1(fname, 
        laserlims=[1550., 1630.],
        trans_cut=1., wave_cut=.3, 
        cal=[1., 0.],
        debug=False):

    # load data depending on how it is supplied
    try:
        ext = fname[-3:]
    except:
        ext = "data"
    
    if ext == "data":
        trans, wls = fname
    elif ext == "npy":
        trans, wls = load(fname).T
    else:
        trans, wls = loadtxt(fname).T
        
    trans = cal[0] * trans + cal[1]



    if len(laserlims) != 2:
        raise RuntimeError("expect laser lims to be 2 elements long")
    start, stop = laserlims[0], laserlims[1]

    # convert volts to wavelengths and smooth
    wls = start + wls / 10. * (stop - start)
    wl_sample = (max(wls) - min(wls)) / len(wls)
    
    b, a = signal.bessel(4, wl_sample / wave_cut * 2.)
    wls = signal.filtfilt(b, a, wls)

    # smooth transmission if wanted
    if trans_cut < 1.:
        raise RuntimeError("trans cut not implemented yet")


    # sort? numpy.interp requires srted data
    sort = argsort(wls)
    wls = wls[sort]
    trans = trans[sort]
    del sort


    # reinterpolate
    scanf = make_interpolator(wls, trans)
    scanf.n = len(wls)

    if debug:
        return scanf, {
            'trans': trans,
            'wls': wls,
        }
    else:
        return scanf


def test_wavevolt(fname, t0=0, t1=-1, trans_cut=1., wave_cut=0.0015):
    if fname[-3:] == "npy":
        trans0, wls0 = load(fname).T
    else:
        trans0, wls0 = loadtxt(fname).T

    # smooth volt wavelength data
    if wave_cut < 1.:
        window = signal.firwin(1e4 + 1, wave_cut)
        wls1 = signal.fftconvolve(wls0, window, mode='same')
    else:
        wls1 = wls0

    if trans_cut < 1.:
        window = signal.firwin(1e4 + 1, trans_cut)
        trans1 = signal.fftconvolve(trans0, window, mode='same')
    else:
        trans1 = trans0

    return wls0, wls1
